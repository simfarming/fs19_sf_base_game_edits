@ECHO off

SET filename="FS19_SF_Base_Game_Edits.zip"

IF EXIST %filename% (
    DEL  %filename% > NUL
)

"7z.exe" a -tzip %filename% ^
#   -i!*.lua ^
#   -i!*.dds ^
#   -i!*.i3d ^
#   -i!*.i3d.shapes ^
#   -i!*.i3d.anim ^
#   -i!*.ogg ^
    -i!*.xml ^
#   -aoa -r ^

IF %ERRORLEVEL% NEQ 0 ( exit 1 )

PAUSE